using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace homework_I.Pages
{
    public class servicesModel : PageModel
    {
        public string titlePage { get; set; }
        public string arrayObject { get; set; }
        public void OnGet()
        {
            string [] arrayObject = { "Software Development", "Outsourcing" , "Quality Assurance" };
            ViewData["arrayObject"] = arrayObject;
        }
    }
}