using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace homework_I.Pages
{
    public class OutsourcingModel : PageModel
    {
        public string titlePage { get; set; }
        public string description { get; set; }
        public void OnGet()
        {
            description = "In business, outsourcing is an agreement in which one company contracts-out a part of existing internal activity to another company. It involves the contracting out of a business process (e.g. payroll processing, claims processing) and operational, and/or non-core functions (e.g. manufacturing, facility management, call center support) to another party (see also business process outsourcing). The concept outsourcing came from the American Glossary 'outside resourcing' and it dates back to at least 1981.";
        }
    }
}