using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace homework_I.Pages
{
    public class QualityAssuranceModel : PageModel
    {
        public string titlePage { get; set; }
        public string description { get; set; }
        public void OnGet()
        {
            description = "Quality assurance (QA) is a way of preventing mistakes or defects in manufactured products and avoiding problems when delivering solutions or services to customers; which ISO 9000 defines as part of quality management focused on providing confidence that quality requirements will be fulfilled. This defect prevention in quality assurance differs subtly from defect detection and rejection in quality control, and has been referred to as a shift left as it focuses on quality earlier in the process.";
        }
    }
}